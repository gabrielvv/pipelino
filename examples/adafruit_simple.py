from Adafruit_IO import Client
import os

ADAFRUIT_IO_KEY = os.getenv('ADAFRUIT_IO_KEY')
aio = Client(ADAFRUIT_IO_KEY)
aio.send('pipelino', 42)

# Now read the most recent value from the feed 'Test'.  Notice that it comes
# back as a string and should be converted to an int if performing calculations
# on it.
data = aio.receive('pipelino')
print('Retrieved value from pipelino has attributes: {0}'.format(data))
print('Latest value from pipelino: {0}'.format(data.value))
