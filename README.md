# PIPELINO

![architecture](./images/pipelino.png)

**Objectif:**  
Le but de ce projet est de suivre l’état d’avancement d’un pipeline de déploiement continu sur Gitlab.

**Fonctionnement:**  
Lors d’un push sur [Gitlab](https://gitlab.com/) des données sont envoyées via des webhooks sur [heroku](https://dashboard.heroku.com) puis envoyées à leurs tour en MQTT via [io.adafruit](https://io.adafruit.com) sur le servomoteur pour afficher l’état d’avancement du déploiement en  4 états : Start - Push - Test - Deploy.

## Références

* Gitlab API
  * [Webhooks](https://docs.gitlab.com/ce/user/project/integrations/webhooks.html#example-webhook-receiver)
* Gitlab CI
  * [Test and Deploy Python App](https://gitlab.com/help/ci/examples/test-and-deploy-python-application-to-heroku.md)
* Heroku
  * [WSGI](http://sametmax.com/quest-ce-que-wsgi-et-a-quoi-ca-sert/)
  * https://devcenter.heroku.com/articles/getting-started-with-python#set-up
  * https://github.com/datademofun/heroku-basic-flask
* Python
  * [Adafruit IO](https://github.com/adafruit/io-client-python)
  * [gunicorn](http://gunicorn.org/)
  * [flask](https://www.palletsprojects.com/p/flask/)
  * [flask.Request](http://flask.pocoo.org/docs/0.12/api/#flask.Request)
  * [Flask-Testing](http://flask.pocoo.org/docs/0.12/testing/)
  * [pytest](https://docs.pytest.org/en/latest/contents.html)

## Images

#### Montage
![montage1](./images/pipelino_montage1.jpg)
![montage2](./images/pipelino_montage2.jpg)  

![montage3](./images/pipelino_montage3.png)

#### Pipeline Gitlab
![gitlab](./images/pipelino_gitlab.png)

#### Console Heroku
![heroku](./images/pipelino_heroku.png)

## Mémo

```sh
heroku config:set ADAFRUIT_IO_KEY=<api_key>
heroku config:set GITLAB_ACCESS_TOKEN=<token>
heroku config:set TRIGGER_TOKEN=<token>
```

```sh
# Windows terminal => set FLASK_APP=app.py
# Windows Powershell => $env:FLASK_APP="app.py"
export ADAFRUIT_IO_KEY=<api_key>
export FLASK_APP=app.py
flask run
```
