#include <ESP8266HTTPClient.h>

#include <AceButton.h>
using namespace ace_button;
#include <AdjustableButtonConfig.h>
#include <ButtonConfig.h>
#include <ESP8266WiFi.h>

#define PIN_BUTTON D3

/************************* WiFi Access Point *********************************/

#define WLAN_SSID       ...WLAN_SSID...
#define WLAN_PASS       ...WLAN_PASS...

/************ Global State (you don't need to change this!) ******************/

// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;

/*************************** Sketch Code ************************************/

AceButton btn;
ButtonConfig buttonConfig;

void handleEventButton(AceButton* button, uint8_t eventType, uint8_t buttonState) {

  if(eventType != AceButton::kEventPressed)
    return;

  Serial.println("Click");
  HTTPClient http;
  http.begin("https://salty-hollows-36429.herokuapp.com/push");
  // start connection and send HTTP header
  int httpCode = http.GET();
  http.end();
}

void setup() {

  pinMode(PIN_BUTTON, INPUT_PULLUP);

  // button set eventHandler;
  buttonConfig.setFeature(ButtonConfig::kFeatureClick);
  buttonConfig.setEventHandler(handleEventButton);
  btn.setButtonConfig(&buttonConfig);
  btn.init(PIN_BUTTON);

  Serial.begin(115200);
  delay(10);

  Serial.println(F("Pipelino Push Button"));

  // Connect to WiFi access point.
  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);

  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.println("WiFi connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());

}

void loop() {
  btn.check();
}
