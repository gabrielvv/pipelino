from flask import Flask, request, url_for, redirect
import requests
import time
import json
import os
from Adafruit_IO import Client

app = Flask(__name__)

ADAFRUIT_IO_KEY = os.getenv('ADAFRUIT_IO_KEY')
GITLAB_ACCESS_TOKEN = os.getenv('GITLAB_ACCESS_TOKEN')
TRIGGER_TOKEN = os.getenv('TRIGGER_TOKEN')
MQTT_PATH = "pipelino"

def get_progress(kind, branch, step, status):
    print(kind, branch, step, status)
    if branch == 'master':
        if kind == 'push':
            return (2/4 - 1/8) * 100;
        elif kind == 'pipeline':
            if status == 'pending':
                return (1/4 - 1/8) * 100;
            if step == 'test' and status == 'success':
                return (3/4 - 1/8) * 100;
            if step == 'deploy' and status == 'success':
                return (4/4 - 1/8) * 100;

    return (1/4 - 1/8) * 100;

@app.route("/push", methods=['GET'])
def push():
    payload = {
        'token': TRIGGER_TOKEN
    }
    requests.post("https://gitlab.com/api/v4/projects/6100679/ref/master/trigger/pipeline", params=payload);
    print("push request")
    return "200"

@app.route("/", methods=['GET', 'POST'])
def hook():
    # http://flask.pocoo.org/docs/0.12/api/#flask.Request
    if request.method == 'POST':
        if request.data:
            data = json.loads(request.data)
            print(data)

            kind = data['object_kind']
            branch = "unknown"
            status = "failed"
            step = "unknown"

            if kind == "pipeline":
                branch = data['object_attributes']['ref']
                status = data['object_attributes']['status']
            elif kind == "push":
                branch = data['ref'].split('/')[2]
            elif kind == "build":
                branch = data['ref']
                step = data['build_stage']
                status = data['build_status']


            aio = Client(ADAFRUIT_IO_KEY)
            aio.send(MQTT_PATH, get_progress(kind, branch, step, status))

    if request.method == 'GET':
        return redirect(url_for('static', filename='index.html'))

    return "200"

if __name__ == "__main__":
    app.run(debug=True, use_reloader=True)
